/**
 *  SSC_SI4432_Client.h
 */

#pragma once
#include "SI4432_Client.h"
#include "SSC_Record.h"
// REMOVED #include "SSC_SI4432_Ant1_Client.h"
// #include "SSC_SI4432_Ant2_Client.h"
// #include "SSC_SI4432_Ant3_Client.h"

/**
 * @ingroup SpySeeCanCSPU
 * @brief This class takes care of the interaction with the SI4432 module in the specific context of the SpySeeCan projet.
 * It collects the data into an SSC_Record.
 * It is an instance for the SpySeeCan project.
 */

class SSC_SI4432_Client : public SI4432_Client
{
public:
    /**
       @brief Constructor
       @param SI4432_CS The number of the pin used for the SI4432's chip select.
    */
    SSC_SI4432_Client(uint8_t SI4432_CS);
    /**
     * @brief Initialise the client before use.
     */
    bool begin();
    /** @brief Read sensor data and populate data record.
        @param record The data record to populate (fields ...).
        @return True if reading was successful.
    */
    bool readData(SSC_Record &record);

private:
   // REMOVED  SSC_SI4432_Ant1_Client ant1;
   // REMOVED  powerDBm_t power[SSC_Record::RF_POWER_TABLE_SIZE];
    uint8_t cycleIndex;   /**< The index of the next record in the measurement cycle. Cycle is defined in SSC_Config.h */
};
