/*
 * IMU_LSM_CalibrationData.h
 *
 */

#pragma once
#include "IsaTwoConfig.h"

/** The structure containing all calibration parameters for the LSM IMU */

#if IMU_REFERENCE==LSM_A
const struct IMU_Calib {
  float accelZeroG[3] =  { -155.06054687f,35.37988281f,582.66015625f};
  float accelResolution[3] =  { -0.00058489f,-0.00058631f,-0.00059821f};
  float gyroZeroRate[3] =  { 12.13999938f,-4.11999988f,-670.45996093f};
  float gyroResolution[3] =  { 0.00875f,0.00875f,0.00875f};
  float magOffset[3] = { -1.47f, -7.77f, -11.29f};
  float magTransformationMatrix[3][3]= {
        { 0.986f , -0.032f, 0.006f},
          { -0.032f, 0.938f , 0.005f },
          { 0.006f, 0.005f, 1.082f }};

} IMU_Calib;

#elif IMU_REFERENCE==LSM_B
// LSMB. Calib Accel+Gyro 21/3 + Mag 23/3 using motion cal (offsets times 10).
const struct IMU_Calib {
	float accelZeroG[3] =  { -357.51953125f,-69.74023438f,558.20019531f};
	float accelResolution[3] =  { 0.00058484f,0.00058635f,0.00059519f};
	float gyroZeroRate[3] =  { 146.02000427f,-46.58000183f,-599.17999268f};
	float gyroResolution[3] =  { 0.00875000f,0.00875000f,0.00875000f};
	float magOffset[3] = { -16.52f, -5.60f, -3.18f};
	float magTransformationMatrix[3][3]= {
			  { 0.997f , -0.089f, -0.014f},
		      { -0.089f, 0.964f , 0.024f },
		      { -0.014f, 0.024f, 1.082f }};

} IMU_Calib;
#elif IMU_REFERENCE==LSM_C
// LSM_C = operational one integrated in final HW .
// *** WARNING Currently just a copy of LSM_B. TO BE COMPLETED.
const struct IMU_Calib {
	float accelZeroG[3] =  { -357.51953125f,-69.74023438f,558.20019531f};
	float accelResolution[3] =  { 0.00058484f,0.00058635f,0.00059519f};
	float gyroZeroRate[3] =  { 146.02000427f,-46.58000183f,-599.17999268f};
	float gyroResolution[3] =  { 0.00875000f,0.00875000f,0.00875000f};
	float magOffset[3] = { -16.52f, -5.60f, -3.18f};
	float magTransformationMatrix[3][3]= {
			  { 0.997f , -0.089f, -0.014f},
		      { -0.089f, 0.964f , 0.024f },
		      { -0.014f, 0.024f, 1.082f }};

} IMU_Calib;
#else
#error "Unexpected IMU_REFERENCE in IMU_LSM_CalibrationData.h !"
#endif
