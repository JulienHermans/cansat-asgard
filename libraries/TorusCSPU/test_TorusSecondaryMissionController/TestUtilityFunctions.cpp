/*
 * TestUtilityFunctions.cpp
 *
 */

#include "TestUtilityFunctions.h"
#include "CansatConfig.h"

void RunA_RecordForRefAltitudeTest(TorusRecord& record, float altitude) {
	record.altitude=altitude+(random(0,150)-75.0f)/100.0f;
	record.timestamp+=CansatAcquisitionPeriod;
	controller.run(record);
	recordCounter++;
	if ((recordCounter % 100) == 0) {
		Serial << ".";
	}
}

void printSecondaryInfo(TorusRecord& rec) {
	float sec= (rec.timestamp % 60000) / 1000.0f;
	uint16_t min = rec.timestamp / 60000;
	uint8_t  hour = min / 60;
	min = min % 60;

	Serial << hour << ":" << min << ":" << sec << ", ts=" <<rec.timestamp << ", alt.=" << rec.altitude << ", ref.alt=" << rec.refAltitude
		   << ", descVelocity=" << rec.descentVelocity << ", ";
	Serial << "rope=" << rec.parachuteRopeLen << ", tgt=" << rec.parachuteTargetRopeLen
		   <<",ctrlInfo=" << (int) rec.controllerInfo << ", phase=" << rec.flightPhase << ENDL;
}

/** Check controller info in record against expected values. Phase is not checked if 255 */
void checkCtrlInfo(TorusRecord& record,TorusControlCode ctrlCode, const char* ctrlCodeMsg, uint8_t phase) {
	if (record.controllerInfo!=ctrlCode){
		printSecondaryInfo(record);
		Serial << "*** Error: controller code is NOT " << ctrlCodeMsg << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
	if ((phase != 255) && (record.flightPhase!=phase)){
		printSecondaryInfo(record);
		Serial << "*** Error:  flightPhase is NOT " << phase << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
}

/** Check whether flight phase is either phase1 or phase2 (used to support phase detectiong delay) */
void checkPhase(TorusRecord& record, uint8_t phase1, uint8_t phase2) {
	if ((record.flightPhase!=phase1) && (record.flightPhase!=phase2)){
		printSecondaryInfo(record);
		Serial << "*** Error:  flightPhase is NOT " << phase1  << " nor " << phase2 << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
}

void checkTarget(TorusRecord& record, uint16_t target) {
	static uint16_t previousTarget=60000;
	static uint32_t lastChangeTS=0;
	if (record.parachuteTargetRopeLen!=target){
		if ((record.parachuteTargetRopeLen!=previousTarget) ||
				((lastChangeTS -record.timestamp) <= TorusMinDelayBetweenTargetUpdate)) {
			// In this case, the delay in target update is abnormal

			Serial << "*** Error: target=" << record.parachuteTargetRopeLen << ", expected " << target
					<< ", TS=" << record.timestamp << ", lastChange=" <<lastChangeTS << ENDL;
			numErrors++;
			if (stopAfterFirstFailedCheck) {
				Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
				delay(500);
				exit(-1);
			}
		}
	} // if != target
	if (record.parachuteTargetRopeLen != previousTarget) {
		previousTarget=record.parachuteTargetRopeLen;
		lastChangeTS=record.timestamp;
	}
}

void wobbleAltitude(TorusRecord& record){
	constexpr float altitudeWobbling=1.1*NoAltitudeChangeTolerance;
	static bool add=false;
	if (add) record.altitude+=altitudeWobbling;
	else record.altitude-=altitudeWobbling;
	add=!add;
}

void waitEndOfStartup() {
	Serial << "Waiting until start up is over (" << TorusMinDelayBeforeFlightControl/1000.0
		   << " sec. since startup, currently " << millis()/1000.0 << ")...";
	while (millis() < TorusMinDelayBeforeFlightControl) {
		delay(500);
		Serial << "." ;
	}
	Serial << " OK" << ENDL;
}

void setReferenceAltitude(TorusRecord& record){
	Serial << "Setting ref. altitude to " << record.altitude << "m..." ;
	while (fabs(record.refAltitude-record.altitude) > 0.1) {
		controller.run(record);
		record.timestamp+=CansatAcquisitionPeriod;
		recordCounter++;
		if ((recordCounter % 100)==0) Serial << "." ;
	}
	Serial << " OK" << ENDL;
}

void printTestSummary() {
	Serial << ENDL << "===== Test over (processed " << recordCounter << " records). Errors so far: "
			<< numErrors  << " =====" << ENDL;
	if (numErrors && stopAfterFailedTest) {
		Serial << "Stopping because of 'stopAfterFailedTest' setting in test program" << ENDL;
		delay(500);
		exit(-1);
	}
}
