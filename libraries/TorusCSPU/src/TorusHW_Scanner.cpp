/*
 * TorusHWScanner.cpp
 */

#include "TorusHW_Scanner.h"

TorusHW_Scanner::TorusHW_Scanner(const byte anUnusedAnalogInPin)
: CansatHW_Scanner(anUnusedAnalogInPin)
{}

void TorusHW_Scanner::init(const byte SD_CardCS,
		const byte firstI2C_Address, const byte lastI2C_Address,
		const byte RF_SerialPort, const byte GPS_SerialPort,
		const uint16_t wireLibBitRateInKhz)
{
	CansatHW_Scanner::init( SD_CardCS, firstI2C_Address,  lastI2C_Address,
							RF_SerialPort,  GPS_SerialPort, wireLibBitRateInKhz);
	winch.begin(ServoWinchPWM_PinNbr, ServoWinch_mmPerMove);
}

void TorusHW_Scanner::printProjectDiagnostic(Stream& stream) const {
	stream << "Servo-winch expected on pin #" << ServoWinchPWM_PinNbr << " speed: " << ServoWinch_mmPerMove
			<< " mm/" << AsyncServoWinch::updateFrequency << " msec" << ENDL;
}
