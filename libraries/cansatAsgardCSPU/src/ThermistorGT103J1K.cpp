#include "ThermistorGT103J1K.h"


  static constexpr float a0 = 6.22661505227800E+01 ; 
  static constexpr float a1 = -5.82880135939400E-03 ;
  static constexpr  float a2 = 2.80136834189500E-07 ;
  static constexpr  float a3 = -8.22320648568200E-12 ;
  static constexpr float a4 = 1.28442854903800E-16 ;
  static constexpr float a5 = -8.18026720027400E-22 ;
  
 float ThermistorGT103J1K::readTemperature() const {
    float r=readResistance();
  return (((((a5*r+a4)*r+a3)*r+a2)*r+a1)*r+a0);  // constantes = équation de la droite de régression..

  }
