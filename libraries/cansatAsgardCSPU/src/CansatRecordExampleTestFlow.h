/*
 * CansatRecordExampleTestFlow.h
 */

#pragma once
#include "CansatRecordTestFlow.h"
#include "CansatRecordExample.h"

/** Example class for the use of a subclass of CansatRecordTestFlow
 */
class CansatRecordExampleTestFlow: public CansatRecordTestFlow {
public:
	CansatRecordExampleTestFlow(uint16_t theBufferSize=256) : CansatRecordTestFlow(theBufferSize) {};
	virtual ~CansatRecordExampleTestFlow() {};
protected:
	virtual bool populateSecondaryMissionData(CansatRecord& record,  char* ptr) {
		CansatRecordExample& myRec=(CansatRecordExample&) record;

		ptr=strtok(nullptr,",");
		if (!ptr) return false;
		myRec.integerData =atoi(ptr);

		ptr=strtok(nullptr,",");
		if (!ptr) return false;
		myRec.floatData =atof(ptr);

		return true;
	};

};

