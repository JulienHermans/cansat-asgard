/**
 Test for the receiving part of CansatXBeeClient.h (using the ground-side XBee module)
 Test with Feather M0 board.

 Wiring      : see test_XBeeClient-Send sketch (but using the ground-side XBee module)
 Instructions: see test_CansatXBeeClient-send sketch

 */

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "This program only works on Feather MO_Express"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include "CansatRecordExample.h"
#include <type_traits>

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL); // Defined in CansatConfig.h
CansatRecordExample myRecord;
char myString[xbc.MaxStringSize+10]; // AAAA
CansatFrameType stringType;
bool isRecord;

void setup() {
	Serial.begin(115200);
	while (!Serial) {
		;
	}
	digitalWrite(LED_BUILTIN, HIGH);
	Serial << "***** RECEIVER SKETCH (testing CansatXBeeClient) *****" << ENDL;
	Serial << "Initializing Serials and communications..." << ENDL;
	RF.begin(115200);
	xbc.begin(RF); // no return value
	Serial << "Initialization over. " << ENDL;
	Serial << "Test record size = " << sizeof(myRecord) << " bytes, base class size = "
		   << sizeof(CansatRecord) << " bytes." << ENDL;
	Serial << "Test record address: " << (uint32_t) &myRecord << ENDL;

	myRecord.print(Serial);

	delay(1000);

}

void loop() {

	static uint32_t counter = 0;
	counter++;

	myRecord.altitude=-5;
	myRecord.integerData=222;
  uint8_t seqNbr;
	if (xbc.receive(myRecord, myString, stringType, seqNbr, isRecord)) {
		counter=0;
		if (isRecord) {
			Serial << "Received a record. ";
			if (myRecord.checkValues()) {
				Serial << " content is ok!" << ENDL;
			} else {
				Serial << " *** Content NOT ok: " << ENDL;
				myRecord.print(Serial);
			}
		} else {
			Serial << "Received a string, with type=" << (int) stringType
					<< " and sequence number = " << seqNbr << ENDL;
			Serial << "'" << myString << "'" << ENDL;
			if (stringType != CansatRecordExample::TestStringType) {
				Serial << "*** Error: string type should be "
						<< (int) CansatRecordExample::TestStringType << ENDL;
			}
      if (seqNbr != CansatRecordExample::SequenceNumber) {
        Serial << "*** Error: string sequence number should be "
            << CansatRecordExample::SequenceNumber << " (got " << seqNbr << ")" << ENDL;
      }

			if (strcmp(myString, CansatRecordExample::testString)) {
				Serial << "*** Error: " << ENDL;
				Serial << "  Should be: '" << CansatRecordExample::testString << ENDL;
			}
		}
	}
	if (counter > 600000) {
		counter = 0;
		Serial << "Still alive (but not receiving anything)..." << ENDL;
	}
}
