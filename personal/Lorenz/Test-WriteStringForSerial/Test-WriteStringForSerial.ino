const int timeOut = 10000;

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(timeOut);
}

void loop() {
  while (Serial.available() == 0) {
    ;
  }
  String string = Serial.readString();
  while (Serial.available() != 0) {
    Serial.read(); // Discard additional chars
  }
  Serial.println(string);
}

