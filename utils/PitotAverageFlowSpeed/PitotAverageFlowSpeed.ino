/*
   PitotAverageFlowSpeed.ino
*/

/** @file
    A utility program to measure airflow velocity using a Pitot tube and a SDP8XX differential pressure sensor.
    This program provides a reading every ReadingPeriod msec, which is the average of all readings taken during
    the last period.
    The number of averaged readings is limited by the µController speed and sensor response time only.
*/

#include <Wire.h>
#include <sdpsensor.h>
#include "elapsedMillis.h"
#include "DebugCSPU.h"

const uint32_t ReadingPeriod = 1000; // The reading period in msec.
const bool useRho100 = true;		 // set to true to used air density at 100 m (adequate for our windTunnel)


const float rho0 = 1.225;			 // air density in kg/m^3 at sea level
const float rho100 = 1.213;		 // air density in kg/m^3 at 100 m
const float rho = (useRho100 ? rho100 : rho0); 	// the actual air density used by this program.
const uint16_t LoopDelay = 0; // msec. Delay to slow down the loop
const bool DebugSamples = false;

SDP8XXSensor sdp;			   // The interface object to the pressure sensor.
uint32_t sampleCounter = 0;    // The counter of samples, incremented after each sample is added to
								// sumOfSamples, so we now how to computer the average.
double sumOfSamples = 0.0;     // The sum of all samples read during the period.
elapsedMillis elapsedSinceAverage;		   // The duration elapsed since the last averaging.
elapsedMillis elapsed;		   // The duraction elapsed since the program started.

void readValue() {
  if (!sdp.readSample())
  {
	float Dp = sdp.getDifferentialPressure();
    float v = sqrt(2 * fabs(Dp) / rho);
    sumOfSamples += v;
    sampleCounter++;

    if (DebugSamples) Serial << "  " << sampleCounter << ": Dp=" << Dp << ", v=" << v << ",sum=" << sumOfSamples << ENDL;
  }
  else Serial << "*** Error reading sample " << ENDL;
}

void printAverageAndReset() {
  Serial << elapsed / 1000.0 << ": velocity=" << sumOfSamples / sampleCounter << " m/s (" << sampleCounter << " samples)" << ENDL;
  sumOfSamples = 0.0;	// Reset average
  sampleCounter = 0;	// Reset sample counter
  elapsedSinceAverage = 0;		// reset timer.
}

void setup() {
  Wire.begin();
  Serial.begin(115200);    // BDH: toujours travailler à 115200 par défaut, qu'on ne doive pas constamment changer le réglage du terminal
  while (!Serial) ;		   // Wait for serial port initialization
  Serial << ENDL << "Pitot air flow velocity measurement utility" << ENDL;

  do {
    int ret = sdp.init();
    if (ret == 0) {
      Serial.print("  SDP init(): OK\n");
      break;
    } else {
      Serial.print("  SDP init(): failed, ret = ");
      Serial.println(ret);
      delay(1000);
    }
  } while (true);
  Serial  <<  "  Starting... readings are averaged every " << ((float) ReadingPeriod) / 1000.0 << " second." << ENDL;
  Serial  <<  "  Air density used: " << rho << " kg/m^3";
  if (useRho100) {
    Serial << " (density at 100 m)" << ENDL;
  }  else {
    Serial << " (density at sea level)" << ENDL;
  }
  Serial  << ENDL;
  elapsedSinceAverage = 0;
  elapsed = 0;
}

void loop() {
  // 1. Read a value at each loop.
  readValue();
  // 2. If the period is over, print the average and reset.
  if (elapsedSinceAverage >= ReadingPeriod) {
    printAverageAndReset();
  }
  delay(LoopDelay);
}
