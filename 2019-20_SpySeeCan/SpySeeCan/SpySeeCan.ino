/*
   SpySeeCanCSPU main software file
*/

#include "SSC_Config.h"
#include "SSC_AcquisitionProcess.h"

SSC_AcquisitionProcess process;

void setup() {
  DINIT_IF_ANALOG_PIN(USB_SerialBaudRate, DebugCtrlPinNumber);
  DPRINTS(DBG_SETUP, "Analog read of the debug switch: ");
  DPRINTLN(DBG_SETUP, analogRead(DebugCtrlPinNumber));
  
  DPRINTSLN(DBG_SETUP, "SpySeeCanCSPU");

  process.init();
#ifdef START_CAMPAIGN_IMMEDIATELY
  process.startMeasurementCampaign("Test SSC_AcquisitionProcess.h");
#endif
  DPRINTSLN(DBG_SETUP, "Setup complete");
}

void loop() {
  DPRINTSLN(DBG_LOOP, "*** Entering processing loop ***");
  process.run();
  DDELAY(DBG_LOOP, 500); // Slow main loop down for debugging
  DPRINTSLN(DBG_LOOP, "*** Exiting processing loop ***");
}
